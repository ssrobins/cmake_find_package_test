cmake_minimum_required(VERSION 3.15)

project(find_package_test)

set(CMAKE_FIND_ROOT_PATH ${CMAKE_SOURCE_DIR} ${CMAKE_FIND_ROOT_PATH})

find_package(BZip2)

find_package(ZLIB)


